CC=gcc
CXX=g++
CFLAGS=-g -Wall
CXXFLAGS=-g -std=c++17 -Wall

hoc_cpp_1.tab.cc: hoc_cpp_1.yy
	bison -d hoc_cpp_1.yy
hoc_cpp_1: hoc_cpp_1.tab.cc
	g++ hoc_cpp_1.tab.cc -o hoc_cpp_1


hoc_cpp: hoc_cpp.o
	$(CXX) $(CXXFLAGS) $^ -o $@

hoc_cpp.o: hoc_cpp.cpp
	$(CXX) $(CXXFLAGS) -c $<
	
hoc_cpp.cpp: hoc_cpp.yy
	bison -o hoc_cpp.cpp hoc_cpp.yy

y.tab.h: c.y
	bison -y -d c.y
y.tab.c: c.y
	bison -y -d c.y

y.tab.cpp: y.tab.c
	mv $< $@

y.tab.o: y.tab.cpp
	$(CXX) $(CXXFLAGS) -c $<

#y.tab.o: y.tab.c
#	$(CC) -c $<

lex.yy.cpp: c.l
	flex -o $@ $<

#lex.yy.cpp: lex.yy.c
#	mv $< $@

#lex.yy.o: lex.yy.c y.tab.h
#	$(CC) -c $<

lex.yy.o: lex.yy.cpp y.tab.h
	$(CXX) $(CXXFLAGS) -c $<

c: y.tab.o lex.yy.o
	$(CXX) $(CXXFLAGS) $^ -o $@

hoc_arithmetic: hoc_arithmetic.o
	$(CC) $(CFLAGS) $< -o $@

hoc_arithmetic.c: hoc_arithmetic.y
	bison -y $< -o $@

hoc: hoc.c
	$(CC) $(CFLAGS) $< -o $@
hoc.c: hoc.y 
	bison -y hoc.y -o $@

hoc_if: hoc_if.c
	$(CC) $(CFLAGS) $< -o $@

hoc_if.c: hoc_if.y 
	bison -y $< -o $@

hoc_if_ast: hoc_if_ast.cpp
	$(CXX) $(CXXFLAGS) $< -o $@
hoc_if_ast.cpp: hoc_if_ast.y 
	bison -y $< -o $@
clean:
	rm -rf y.tab.c y.tab.h  y.tab.cpp lex.yy.cpp hoc hoc_cpp_1.tab.cc hoc_cpp_1.tab.hh
