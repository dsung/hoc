%require "3.2"
%debug
%language "c++"
%define api.token.constructor
%define api.value.type variant
%define api.location.file none
%define parse.assert
%locations

%code requires // *.hh
{
#include <string>
#include <vector>
typedef std::vector<std::string> strings_type;
}

%code // *.cc
{
#include <iostream>
#include <sstream>

  namespace yy
  {
    // Prototype of the yylex function providing subsequent tokens.
    static parser::symbol_type yylex ();

    // Print a vector of strings.
    std::ostream&
    operator<< (std::ostream& o, const strings_type& ss)
    {
      o << '{';
      const char *sep = "";
      for (strings_type::const_iterator i = ss.begin (), end = ss.end ();
           i != end; ++i)
        {
          o << sep << *i;
          sep = ", ";
        }
      return o << '}';
    }
  }

  // Convert to string.
  template <typename T>
    std::string
    to_string (const T& t)
  {
    std::ostringstream o;
    o << t;
    return o.str ();
  }
}

%token <int> NUMBER;
%token <char> CHAR;
%token END_OF_FILE 0;
%token
  ASSIGN  ":="
  PLUS    "+"
  MINUS   "-"
  MUL     "*"
  DIV     "/"
  LPAREN  "("
  RPAREN  ")"
  NEWLINE  "\n"
;

%type <int> list;
%type <int> expr;

%left "+" "-"
%left "*" "/"

%%

list:    {printf("\taaempty\n");}
     | list "\n" {printf("list \\n\n");}
     | list expr "\n" { printf("%d\n", $2); }

expr: NUMBER {$$ = $1; printf("xx num %d\n", $1);}
       | expr "+" expr {$$ = $1 + $3;}
       | expr "-" expr {$$ = $1 - $3;}
       | expr "*" expr {$$ = $1 * $3;}
       | expr "/" expr {$$ = $1 / $3;}
       | "(" expr ")"
       {
         $$ = $2;
       }


%%

char *progname;
int lineno = 1;

namespace yy
{
  // Use nullptr with pre-C++11.
#if !defined __cplusplus || __cplusplus < 201103L
# define NULLPTR 0
#else
# define NULLPTR nullptr
#endif

  // The yylex function providing subsequent tokens:
  // TEXT         "I have three numbers for you."
  // NUMBER       1
  // NUMBER       2
  // NUMBER       3
  // TEXT         "And that's all!"
  // END_OF_FILE

  static
  parser::symbol_type
  yylex ()
  {
    int c;
    int input_val;
    static int count = 0;
    const int stage = count;
    ++count;
    parser::location_type loc (NULLPTR, stage + 1, stage + 1);

    while ((c=getchar()) == ' ' || c == '\t')
      ;

    if (c == EOF)
      return parser::make_END_OF_FILE (loc);
    if (c == '.' || isdigit(c) )
    {
      ungetc(c, stdin);
      //scanf("%lf", &input_val);
      scanf("%d", &input_val);
      //val = 5;
      return parser::make_NUMBER (input_val, loc);
    }

    switch (c)
    {
      case '+':
      {
        return parser::make_PLUS(loc);
        break;
      }
      case '-':
      {
        return parser::make_MINUS(loc);
        break;
      }
      case '*':
      {
        return parser::make_MUL(loc);
        break;
      }
      case '/':
      {
        return parser::make_DIV(loc);
        break;
      }
      case '(':
      {
        return parser::make_LPAREN(loc);
        break;
      }
      case ')':
      {
        return parser::make_RPAREN(loc);
        break;
      }
    }

    if (c == '\n')
    {
      ++lineno;
      return parser::make_NEWLINE(loc);
    }
    //return c;
    //return parser::make_CHAR(c, loc);
    char str[2] = {0};
    str[0] = c;
    throw yy::parser::syntax_error (loc, "invalid character: " + std::string(str));
  }

  // Mandatory error function
  void parser::error (const parser::location_type& loc, const std::string& msg)
  {
    std::cerr << loc << ": " << msg << '\n';
  }
}

int main ()
{
  yy::parser p;
  p.set_debug_level (!!getenv ("YYDEBUG"));
  return p.parse ();
}

// Local Variables:
// mode: C++
// End:
