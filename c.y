%{

// https://www.lysator.liu.se/c/ANSI-C-grammar-y.html#declarator

/*
ANSI C Yacc grammar
In 1985, Jeff Lee published his Yacc grammar (which is accompanied by a matching Lex specification) for the April 30, 1985 draft version of the ANSI C standard.  Tom Stockfisch reposted it to net.sources in 1987; that original, as mentioned in the answer to question 17.25 of the comp.lang.c FAQ, can be ftp'ed from ftp.uu.net, file usenet/net.sources/ansi.c.grammar.Z.
Jutta Degener, 1995
*/

#include <unistd.h>
#include <cstring>
#include <string>
#include <vector>
#include <iostream>

using namespace std;

int yyerror(const char *s);
int yylex (void);
int para_num;
int var_num;

#define YYDEBUG 1
char *key;
typedef struct TypeData_
{
  const char *storage_class_;
  int type_specifier_;
  //char *identifier_;
  vector <string> identifier_;
}TypeData;

vector <string> types;
vector <string> g_pointer;
vector <string> identifier;
//vector <pair<string, string>> identifier; // id, especially type mark, ex: id is a function
string cur_type;

TypeData type_data;

%}

%union 
{
  char* id;
  int num;
}

%token <id>   IDENTIFIER CONSTANT

%token STRING_LITERAL SIZEOF 
%token PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token XOR_ASSIGN OR_ASSIGN TYPE_NAME

%token TYPEDEF EXTERN STATIC AUTO REGISTER
%token CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE VOID
%token STRUCT UNION ENUM ELLIPSIS

%token CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

/* %start translation_unit */
%start declaration

%%

primary_expression
	: IDENTIFIER
	| CONSTANT
        {
          printf("\ncc CONSTANT: %s\n", $1);
        }
	| STRING_LITERAL
	| '(' expression ')'
	;

postfix_expression
	: primary_expression
	| postfix_expression '[' expression ']'
	| postfix_expression '(' ')'
	| postfix_expression '(' argument_expression_list ')'
	| postfix_expression '.' IDENTIFIER
	| postfix_expression PTR_OP IDENTIFIER
	| postfix_expression INC_OP
	| postfix_expression DEC_OP
	;

argument_expression_list
	: assignment_expression
	| argument_expression_list ',' assignment_expression
	;

unary_expression
	: postfix_expression
	| INC_OP unary_expression
	| DEC_OP unary_expression
	| unary_operator cast_expression
	| SIZEOF unary_expression
	| SIZEOF '(' type_name ')'
	;

unary_operator
	: '&'
	| '*'
	| '+'
	| '-'
	| '~'
	| '!'
	;

cast_expression
	: unary_expression
	| '(' type_name ')' cast_expression
	;

multiplicative_expression
	: cast_expression
	| multiplicative_expression '*' cast_expression
	| multiplicative_expression '/' cast_expression
	| multiplicative_expression '%' cast_expression
	;

additive_expression
	: multiplicative_expression
	| additive_expression '+' multiplicative_expression
	| additive_expression '-' multiplicative_expression
	;

shift_expression
	: additive_expression
	| shift_expression LEFT_OP additive_expression
	| shift_expression RIGHT_OP additive_expression
	;

relational_expression
	: shift_expression
	| relational_expression '<' shift_expression
	| relational_expression '>' shift_expression
	| relational_expression LE_OP shift_expression
	| relational_expression GE_OP shift_expression
	;

equality_expression
	: relational_expression
	| equality_expression EQ_OP relational_expression
	| equality_expression NE_OP relational_expression
	;

and_expression
	: equality_expression
	| and_expression '&' equality_expression
	;

exclusive_or_expression
	: and_expression
	| exclusive_or_expression '^' and_expression
	;

inclusive_or_expression
	: exclusive_or_expression
	| inclusive_or_expression '|' exclusive_or_expression
	;

logical_and_expression
	: inclusive_or_expression
	| logical_and_expression AND_OP inclusive_or_expression
	;

logical_or_expression
	: logical_and_expression
	| logical_or_expression OR_OP logical_and_expression
	;

conditional_expression
	: logical_or_expression
	| logical_or_expression '?' expression ':' conditional_expression
	;

assignment_expression
	: conditional_expression
	| unary_expression assignment_operator assignment_expression
	;

assignment_operator
	: '='
	| MUL_ASSIGN
	| DIV_ASSIGN
	| MOD_ASSIGN
	| ADD_ASSIGN
	| SUB_ASSIGN
	| LEFT_ASSIGN
	| RIGHT_ASSIGN
	| AND_ASSIGN
	| XOR_ASSIGN
	| OR_ASSIGN
	;

expression
	: assignment_expression
	| expression ',' assignment_expression
	;

constant_expression
	: conditional_expression
	;

declaration
	: declaration_specifiers ';'
	| declaration_specifiers init_declarator_list ';'
        {
        #if 1
          vector<string> var_id, var_types, var_func;


          for (int i=0 ; i < var_num ; ++i)
          {
            string t;
            string pointer;

            if (types.back() == "function")
            {
              var_func.push_back("function");
              types.pop_back();
            }
            else
            {
              var_func.push_back("");
            }

            while (types.back() == "*")
            {
              types.pop_back();
              pointer += "*";
            }

            t = types.back();
            types.pop_back();
            t+= pointer;

            auto id = identifier.back();
            identifier.pop_back();

            var_id.push_back(id);
            var_types.push_back(t);
            cout << "id: " << id << endl;
            cout << "t: " << t << endl;
          }

#if 1
            for (auto &i : types)
            {
              cout << "type: " << i << endl;
            }

            for (auto &i : identifier)
            {
              cout << "id: " << i << endl;
            }
            for (auto &i : var_func)
            {
              cout << "var_func: " << i << endl;
            }
#endif

          for (auto it = var_id.rbegin(), it_t = var_types.rbegin(), it_f = var_func.rbegin() ; it != var_id.rend(); ++it, ++it_t, ++it_f)
          {
            cout << "var: " << *it << ", type: ";

#if 1
            //if (it_f != var_func.rend() && *it_f == "function")
            //if (it_f != var_func.rend() )
            if (*it_f == "function")
            {
              cout << "funciton return ";
            }
#endif
            cout << *it_t << endl;
          }
        #endif
        }
	;

declaration_specifiers
	: storage_class_specifier
	| storage_class_specifier declaration_specifiers
	| type_specifier
          {
            //printf("\nxx type_specifier: %d\n", $1);
          }
	| type_specifier declaration_specifiers
	| type_qualifier
	| type_qualifier declaration_specifiers
	;

init_declarator_list
	: init_declarator
        {
          ++var_num;
          printf("\n111 init_declarator, var_num: %d\n", var_num);
        }
	| init_declarator_list ',' init_declarator
        {
          ++var_num;
          printf("\n222, var_num: %d\n", var_num);
        }
	;

init_declarator
	: declarator
	| declarator '=' initializer
	;

storage_class_specifier
	: TYPEDEF
        {
          type_data.storage_class_ = "TYPEDEF";
          //printf("type_data.identifier_: %s\n", type_data.identifier_);
        }
	| EXTERN
	| STATIC
	| AUTO
	| REGISTER
	;

type_specifier
	: VOID
	| CHAR
        {
          //types.push_back("char");
          printf("\ncc CHAR\n");
          cur_type = "char";
        }
	| SHORT
	| INT
        {
          type_data.type_specifier_ = INT;
          //types.push_back("int");
          printf("\nii INT\n");
          cur_type = "int";
        }
	| LONG
	| FLOAT
	| DOUBLE
	| SIGNED
	| UNSIGNED
	| struct_or_union_specifier
	| enum_specifier
	| TYPE_NAME
        {
            //printf("\nxx TYPE_NAME: %d\n", $1);
            printf("\nxx TYPE_NAME\n");
        }
	;

struct_or_union_specifier
	: struct_or_union IDENTIFIER '{' struct_declaration_list '}'
	| struct_or_union '{' struct_declaration_list '}'
	| struct_or_union IDENTIFIER
	;

struct_or_union
	: STRUCT
	| UNION
	;

struct_declaration_list
	: struct_declaration
	| struct_declaration_list struct_declaration
	;

struct_declaration
	: specifier_qualifier_list struct_declarator_list ';'
	;

specifier_qualifier_list
	: type_specifier specifier_qualifier_list
	| type_specifier
	| type_qualifier specifier_qualifier_list
	| type_qualifier
	;

struct_declarator_list
	: struct_declarator
	| struct_declarator_list ',' struct_declarator
	;

struct_declarator
	: declarator
	| ':' constant_expression
	| declarator ':' constant_expression
	;

enum_specifier
	: ENUM '{' enumerator_list '}'
	| ENUM IDENTIFIER '{' enumerator_list '}'
	| ENUM IDENTIFIER
	;

enumerator_list
	: enumerator
	| enumerator_list ',' enumerator
	;

enumerator
	: IDENTIFIER
	| IDENTIFIER '=' constant_expression
	;

type_qualifier
	: CONST
	| VOLATILE
	;

declarator
	: pointer direct_declarator
        {
          printf("\npp pointer direct_declarator\n");
        }
	| direct_declarator
        {
          printf("\ndd direct_declarator\n");
        }
	;

direct_declarator
	: IDENTIFIER
        {
          type_data.identifier_.push_back($1);
          printf("\nii IDENTIFIER: %s, type_data.storage_class_: %s, type_data.type_specifier_: %d\n", $1, type_data.storage_class_, type_data.type_specifier_);
          identifier.push_back($1);

          cout << "push cur_type: " << cur_type << endl;
          types.push_back(cur_type);
          for (auto &i : g_pointer)
          {
            cout << "push pointer" << endl;
            types.push_back(i);
          }
          g_pointer.clear();


        }
	| '(' declarator ')'
	| direct_declarator '[' constant_expression ']'
        {
          printf("\naa an array direct_declarator [const_expr]\n");
        }
	| direct_declarator '[' ']'
        {
          printf("\nan array direct_declarator []\n");
        }
	| direct_declarator '(' parameter_type_list ')'
        {
          printf("\na function direct_declarator (parameter_type_list)\n");

#if 0
          auto id = identifier.back();
          identifier.pop_back();

            string t;
            string pointer;

              while (types.back() == "*")
              {
                pointer += "*";
                types.pop_back();
              }

              t = types.back();
              types.pop_back();
              t+= pointer;

          cout << "function: " << id << " _return_ " << t << endl;
#endif
          types.push_back("function");
          cout << "function: " << endl;
          para_num = 0;

        }
	| direct_declarator '(' identifier_list ')'
        {
          printf("\ngg direct_declarator (identifier_list)\n");
        }
	| direct_declarator '(' ')'
        {
          printf("\nff direct_declarator ()\n");
        }
	;

pointer
	: '*'
        {
          printf("\nxx pointer\n");
          g_pointer.push_back("*");
        }
	| '*' type_qualifier_list
	| '*' pointer
        {
          printf("\nyy pointer\n");
          g_pointer.push_back("*");
        }
	| '*' type_qualifier_list pointer
	;

type_qualifier_list
	: type_qualifier
	| type_qualifier_list type_qualifier
	;


parameter_type_list
	: parameter_list
        {
          vector<string> para_id, para_types;


          for (int i=0 ; i < para_num ; ++i)
          {
            auto id = identifier.back();
            identifier.pop_back();

            string t;
            string pointer;

              while (types.back() == "*")
              {
                pointer += "*";
                types.pop_back();
              }

              t = types.back();
              types.pop_back();
              t+= pointer;

            para_id.push_back(id);
            para_types.push_back(t);
          }

          cout << endl << identifier.back() <<  " function parameter_list:" << endl;
          for (int i = para_id.size() - 1 ; i >= 0 ; --i)
          {
            cout << "var: " << para_id[i] << ", type: " << para_types[i] << endl;
          }

        }
	| parameter_list ',' ELLIPSIS
        {
          printf("\nuu2 parameter_list, ...\n");
        }
	;

parameter_list
	: parameter_declaration
        {
          //cout << endl << "bef pop_back: " << identifier[identifier.size() - 1] << endl;
          //identifier.pop_back();
          //cout << endl << "after pop_back: " << identifier[identifier.size() - 1] << endl;
        }
	| parameter_list ',' parameter_declaration
        {
          printf("aaa parameter_list ',' parameter_declaration\n");
        }
	;

parameter_declaration
	: declaration_specifiers declarator
        {
          ++para_num;
          printf("\naa declaration_specifiers, para_num: %d\n", para_num);
        }
	| declaration_specifiers abstract_declarator
        {
          //printf("\nbb declaration_specifiers: %d\n", $1);
        }
	| declaration_specifiers
        {
          //printf("\ncc declaration_specifiers: %d\n", $1);
        }
	;

identifier_list
	: IDENTIFIER
	| identifier_list ',' IDENTIFIER
	;

type_name
	: specifier_qualifier_list
	| specifier_qualifier_list abstract_declarator
	;

abstract_declarator
	: pointer
        {
          printf("aa pointer\n");
        }
	| direct_abstract_declarator
	| pointer direct_abstract_declarator
	;

direct_abstract_declarator
	: '(' abstract_declarator ')'
	| '[' ']'
	| '[' constant_expression ']'
	| direct_abstract_declarator '[' ']'
	| direct_abstract_declarator '[' constant_expression ']'
	| '(' ')'
	| '(' parameter_type_list ')'
	| direct_abstract_declarator '(' ')'
	| direct_abstract_declarator '(' parameter_type_list ')'
	;

initializer
	: assignment_expression
	| '{' initializer_list '}'
	| '{' initializer_list ',' '}'
	;

initializer_list
	: initializer
	| initializer_list ',' initializer
	;

statement
	: labeled_statement
	| compound_statement
	| expression_statement
	| selection_statement
	| iteration_statement
	| jump_statement
	;

labeled_statement
	: IDENTIFIER ':' statement
	| CASE constant_expression ':' statement
	| DEFAULT ':' statement
	;

compound_statement
	: '{' '}'
	| '{' statement_list '}'
	| '{' declaration_list '}'
	| '{' declaration_list statement_list '}'
	;

declaration_list
	: declaration
	| declaration_list declaration
	;

statement_list
	: statement
	| statement_list statement
	;

expression_statement
	: ';'
	| expression ';'
	;

selection_statement
	: IF '(' expression ')' statement
	| IF '(' expression ')' statement ELSE statement
	| SWITCH '(' expression ')' statement
	;

iteration_statement
	: WHILE '(' expression ')' statement
	| DO statement WHILE '(' expression ')' ';'
	| FOR '(' expression_statement expression_statement ')' statement
	| FOR '(' expression_statement expression_statement expression ')' statement
	;

jump_statement
	: GOTO IDENTIFIER ';'
	| CONTINUE ';'
	| BREAK ';'
	| RETURN ';'
	| RETURN expression ';'
	;

translation_unit
	: external_declaration
          {
            //printf("\nxx external_declaration: %d\n", $1);
          }
	| translation_unit external_declaration
	;

external_declaration
	: function_definition
	| declaration
	;

function_definition
	: declaration_specifiers declarator declaration_list compound_statement
	| declaration_specifiers declarator compound_statement
	| declarator declaration_list compound_statement
	| declarator compound_statement
	;

%%
#include <stdio.h>

extern char yytext[];
extern int column;

char *progname;
int lineno = 1;


int warning(const char *s, const char *t)
{
  fprintf(stderr, "%s: %s", progname, s);
  if (t)
    fprintf(stderr, " %s", t);

  fprintf(stderr, " near line %d\n", lineno);
  return 0;
}

int yyerror(const char *s)
{
  return warning(s, 0); 
}


int check_type_name(char *str)
{
  string cmp_str{str};

  for (auto &s: type_data.identifier_)
  {
    if (s == cmp_str)
      return 1;
  }
  return 0;
}

int main(int argc, char *argv[])
{
  int opt;
  progname = argv[0];

  while ((opt = getopt(argc, argv, "d:h?")) != -1)
  {
    switch (opt)
    {
      case 'd':
      {
        yydebug = strtol(optarg, 0, 10);
        if (yydebug == 1)
          printf("enable bison debug mode\n");
        break;
      }
    }
  }
  //yydebug = 1;
  yyparse();
}      
