%language "c++"
%require "3.2"
%debug
%define api.value.type variant
%define parse.assert
%locations

%code requires // *.hh
{
#include <string>
#include <vector>
typedef std::vector<std::string> strings_type;

#include "hoc_cpp_1.tab.hh"
}

%code // *.cc
{
#include <iostream>
#include <sstream>

  namespace yy
  {
    int yylex (yy::parser::value_type *yylval, yy::parser::location_type *yylloc);

    // Print a vector of strings.
    std::ostream&
    operator<< (std::ostream& o, const strings_type& ss)
    {
      o << '{';
      const char *sep = "";
      for (strings_type::const_iterator i = ss.begin (), end = ss.end ();
           i != end; ++i)
        {
          o << sep << *i;
          sep = ", ";
        }
      return o << '}';
    }
  }

  // Convert to string.
  template <typename T>
    std::string
    to_string (const T& t)
  {
    std::ostringstream o;
    o << t;
    return o.str ();
  }
}

%token <int> NUMBER;
%token END_OF_FILE 0;
%token
  ASSIGN  ":="
  MINUS   "-"
  PLUS    "+"
  STAR    "*"
  SLASH   "/"
  LPAREN  "("
  RPAREN  ")"
  NEWLINE  "\n"
;

%type <int> list;
%type <int> expr;

%left "+" "-"
%left "*" "/"

%%

list:    {printf("\taaempty\n");}
     | list "\n" {printf("list \\n\n");}
     | list expr "\n" { printf("%d\n", $2); }

expr: NUMBER {$$ = $1; printf("xx num %d\n", $1);}
       | expr "+" expr {$$ = $1 + $3;}
       | expr "-" expr {$$ = $1 - $3;}
       | expr "*" expr {$$ = $1 * $3;}
       | expr "/" expr {$$ = $1 / $3;}
       | '(' expr ')'


%%

char *progname;
int lineno = 1;

namespace yy
{
  // Use nullptr with pre-C++11.
#if !defined __cplusplus || __cplusplus < 201103L
# define NULLPTR 0
#else
# define NULLPTR nullptr
#endif

  // The yylex function providing subsequent tokens:
  // TEXT         "I have three numbers for you."
  // NUMBER       1
  // NUMBER       2
  // NUMBER       3
  // TEXT         "And that's all!"
  // END_OF_FILE

  int yylex (yy::parser::value_type *yylval, yy::parser::location_type *yylloc)
  {
    int c;
    int input_val;
    static int count = 0;
    const int stage = count;
    ++count;
    //parser::location_type loc (NULLPTR, stage + 1, stage + 1);

    while ((c=getchar()) == ' ' || c == '\t')
      ;

    if (c == EOF)
    {
      ;//return parser::make_END_OF_FILE (loc);
      return yy::parser::token::END_OF_FILE;
    }
    if (c == '.' || isdigit(c) )
    {
      ungetc(c, stdin);
      //scanf("%lf", &input_val);
      scanf("%d", &input_val);
      //scanf("%d", yyla->value);
      //val = 5;
      ;//return parser::make_NUMBER (input_val, loc);
      yylval->emplace<int>() = input_val;
      return yy::parser::token::NUMBER;
    }

    switch (c)
    {
      case '+':
      {
        ;//return parser::make_PLUS(loc);
        return yy::parser::token::PLUS;
        break;
      }
      case '-':
      {
        ;//return parser::make_MINUS(loc);
        return yy::parser::token::MINUS;
        break;
      }
    }

    if (c == '\n')
    {
      ++lineno;
      ;//return parser::make_NEWLINE(loc);
      return yy::parser::token::NEWLINE;
    }
      yylval->emplace<int>() = c;
      //return yy::parser::token::NUMBER;
    return c;
    //return parser::make_NUMBER (c, loc);

  #if 0
    static int count = 0;
    const int stage = count;
    ++count;
    parser::location_type loc (NULLPTR, stage + 1, stage + 1);
    switch (stage)
      {
      case 0:
        return parser::make_TEXT ("I have three numbers for you.", loc);
      case 1:
      case 2:
      case 3:
        return parser::make_NUMBER (stage, loc);
      case 4:
        return parser::make_TEXT ("And that's all!", loc);
      default:
        return parser::make_END_OF_FILE (loc);
      }
  #endif 
  }

  // Mandatory error function
  void parser::error (const parser::location_type& loc, const std::string& msg)
  {
    std::cerr << loc << ": " << msg << '\n';
  }
}

int main ()
{
  yy::parser p;
  p.set_debug_level (!!getenv ("YYDEBUG"));
  return p.parse ();
}

// Local Variables:
// mode: C++
// End:
