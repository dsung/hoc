%{
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>

#include <string>
#include <vector>
#include <iostream>

using namespace std;

//#define YYSTYPE double
#define YYDEBUG 1


enum NodeType 
{
  IF_TYPE,
  PLUS,
  MINUS,
  MUL,
  DIV
};

//#define YYSTYPE AstNode

class AstNode
{
  public:
    AstNode(const string str):str_(str), val_(0)
    {
    }
    string str_;
    void set_val(const double val)
    {
      val_ = val;
    }
    int add_child(AstNode *n)
    {
      children_.push_back(n);
      return 0;
    }
    double val_;
    vector<AstNode *> children_;
  private:
    NodeType node_type_;
};

AstNode root{"root"};

static int cnt;

int yylex();
int yyerror(const char *s);
%}


%union
{
  AstNode *node_;
  double num_;
}

%token <num_> NUMBER
%token IF
%left '+' '-'
%left '*' '/'

%type <node_> expr selection_statement

%%


selection_statement: {}
                   | selection_statement IF '(' expr ')' '{' expr ';' '}' '\n'
                   {
                     // if (6-3) {2-(1+5);}
                     AstNode *n = new AstNode{"if"};
                     n->add_child($4);
                     n->add_child($7);
                     cout << "$4->str_: " << $4->str_ << endl;
                     cout << "$7->str_: " << $7->str_ << endl;

                     $$ = n;
                     root.add_child(n);
                     
                     printf("new if/then node, cnt: %d\n", cnt);
                     ++cnt;
                   }

expr: NUMBER 
      {
        AstNode *n = new AstNode{"num"};
        n->set_val($1);
        $$ = n;

      }
       | expr '+' expr 
         {
           AstNode *n = new AstNode{"plus"};
           n->add_child($1);
           n->add_child($3);
           cout << "$1.val: " << $1->val_ << endl;
           cout << "$3.val: " << $3->val_ << endl;
           $$ = n;
           ++cnt;
         }
       | expr '-' expr 
         {
           AstNode *n = new AstNode{"minus"};
           n->add_child($1);
           n->add_child($3);
           cout << "$1.val: " << $1->val_ << endl;
           cout << "$3.val: " << $3->val_ << endl;
           $$ = n;
           ++cnt;
         }
       | '(' expr ')'
       {
        $$ = $2;
       }

%%
char *progname;
int lineno = 1;


int yylex()
{
  int c;
  while ((c=getchar()) == ' ' || c == '\t')
    ;
  
  if (c == EOF)
    return 0;
  if (c == '.' || isdigit(c) )
  {
    ungetc(c, stdin);
    scanf("%lf", &yylval.num_);
    return NUMBER;
  }

  if (c == 'i')
  {
    int ch;

    ch = getchar();
    if (ch == 'f')
      return IF;
    else
    {
      ungetc(ch, stdin);
    }
  }

  if (c == '\n')
    ++lineno;
  return c;  
}

int warning(const char *s, const char *t)
{
  fprintf(stderr, "%s: %s", progname, s);
  if (t)
    fprintf(stderr, " %s", t);

  fprintf(stderr, " near line %d\n", lineno);
  return 0;
}

int yyerror(const char *s)
{
  return warning(s, 0); 
}

void print_node(const AstNode *n)
{
  cout << "n: ("  << n->str_ << ", "<< n->val_ << ")" << endl;
  for (auto i: n->children_)
  {
    print_node(i);
  }
}

void print_tree(const AstNode *n)
{
  if (n->children_.size() == 0) // leaf node
  {
    cout << "("  << n->str_ << ", "<< n->val_;
    cout << ")";
  }
  else
  {
    cout << "("  << n->str_ << ", "<< n->val_;
    for (auto i: n->children_)
    {
      print_tree(i);
    }
    cout << ")";
  }

}


int main(int argc, char *argv[])
{
  int opt;
  progname = argv[0];

  while ((opt = getopt(argc, argv, "d:h?")) != -1)
  {
    switch (opt)
    {
      case 'd':
      {
        yydebug = strtol(optarg, 0, 10);
        if (yydebug == 1)
          printf("enable bison debug mode\n");
        break;
      }
    }
  }

  //yydebug = 1;
  yyparse();
  cout << "\\tree";
  print_tree(&root);
  cout << endl;
}      
