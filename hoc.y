%{
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#define YYSTYPE double
#define YYDEBUG 1

int yylex();
int yyerror(const char *s);
%}

%token NUMBER
%left '+' '-'
%left '*' '/'
%%
list:    {printf("\taaempty\n");}
     | list '\n' {printf("list \\n\n");}
     | list expr '\n' { printf("%.8g\n", $2); }

expr: NUMBER {$$ = $1;}
       | expr '+' expr {$$ = $1 + $3;}
       | expr '-' expr {$$ = $1 - $3;}
       | expr '*' expr {$$ = $1 * $3;}
       | expr '/' expr {$$ = $1 / $3;}
       | '(' expr ')'

%%
char *progname;
int lineno = 1;


int yylex()
{
  int c;
  while ((c=getchar()) == ' ' || c == '\t')
    ;
  
  if (c == EOF)
    return 0;
  if (c == '.' || isdigit(c) )
  {
    ungetc(c, stdin);
    scanf("%lf", &yylval);
    return NUMBER;
  }

  if (c == '\n')
    ++lineno;
  return c;  
}

int warning(const char *s, const char *t)
{
  fprintf(stderr, "%s: %s", progname, s);
  if (t)
    fprintf(stderr, " %s", t);

  fprintf(stderr, " near line %d\n", lineno);
  return 0;
}

int yyerror(const char *s)
{
  return warning(s, 0); 
}

int main(int argc, char *argv[])
{
  int opt;
  progname = argv[0];

  while ((opt = getopt(argc, argv, "d:h?")) != -1)
  {
    switch (opt)
    {
      case 'd':
      {
        yydebug = strtol(optarg, 0, 10);
        if (yydebug == 1)
          printf("enable bison debug mode\n");
        break;
      }
    }
  }

  //yydebug = 1;
  yyparse();
}      
